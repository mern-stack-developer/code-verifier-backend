import express,  { Express, Request, Response } from "express";
import dotenv from 'dotenv';

// Configuration the .env file
//primer paso para hacer uso del archivo .env
dotenv.config();

//Create Express APP
//app sera una aplicacion de tipo express
const app: Express = express();
const port: string | number = process.env.PORT || 8000;

//Define the first route of the APP
app.get('/', (req: Request, res: Response) => {
    //Send Hello World
    res.send('Welcome to APP Restful: Express + TS + Nodemon + Jest + Swagger + Mongoose',);
});

//Define the first route of the APP
app.get('/Hello', (req: Request, res: Response) => {
    //Send Hello World
    res.send('Welcome to GET Route: Hello!!',);
});

//Execute APP and Listen Request to Port
app.listen(port, () => {
    console.log(`Express Server: Running at http://localhost:${port}`)
})