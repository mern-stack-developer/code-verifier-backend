"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
// Configuration the .env file
//primer paso para hacer uso del archivo .env
dotenv_1.default.config();
//Create Express APP
//app sera una aplicacion de tipo express
const app = (0, express_1.default)();
const port = process.env.PORT || 8000;
//Define the first route of the APP
app.get('/', (req, res) => {
    //Send Hello World
    res.send('Welcome to APP Restful: Express + TS + Nodemon + Jest + Swagger + Mongoose');
});
//Define the first route of the APP
app.get('/Hello', (req, res) => {
    //Send Hello World
    res.send('Welcome to GET Route: Hello!!');
});
//Execute APP and Listen Request to Port
app.listen(port, () => {
    console.log(`Express Server: Running at http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map